# To Build:

```sh
git clone https://github.com/bachoseven/urlview
cd urlview/
./configure
automake --add-missing
make
sudo make install
```
