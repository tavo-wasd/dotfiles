.\" .ds FAM T       \" H=Helvetica C=Courier
.\" .nr PS 10p      \" Point size
.\" .nr VS 12p      \" Line spacing
.\" .nr PO 1i       \" Page offset
.\" .nr LL 6i       \" Line length
.\" .nr LT 6i       \" Header/Footer length
.\" .nr HM 1i       \" Header margin
.\" .nr FM 1i       \" Footer margin
.\" .so letter.tmac \" US letter
.\" .so report.tmac \" Cover page
.\" .so utils.tmac  \" General utils
.\" .so toc.tmac    \" Relocate toc
.\" .so md.tmac     \" Md-like syntax
.\" .so math.tmac   \" Math utils
.\" .so apa.tmac    \" Label & accumulate
.\" .so es.tmac     \" Spanish
.
.TL
<title>
.AU
<author>
.AI
<institution>
.
.AB
<abstract>
.AE
.
.NH
.XN "<numbered heading>"
.
.PP
<indented \f[CW]paragraph\f[] \m[blue]with\m[] \f[B]some\f[] \f[I]formatting\f[]>
.
.\" .[
.\" sample_reference
.\" .]
.
.\" .TC
