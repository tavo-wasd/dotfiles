#!/bin/sh
# $ printf '[ -f ~/.config/shell/env ] && . ~/.config/shell/env' | sudo tee -a /etc/profile

export \
    XDG_STATE_HOME="$HOME/.local/state" \
    XDG_CACHE_HOME="$HOME/.local/cache" \
    XDG_DATA_HOME="$HOME/.local/share" \
    XDG_CONFIG_HOME="$HOME/.config" \
    BASH_ENV="$XDG_CONFIG_HOME/shell/bashrc" \
    _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java" \
    MATHEMATICA_USERBASE="$XDG_CONFIG_HOME/mathematica" \
    XCURSOR_PATH=/usr/share/icons:$XDG_DATA_HOME/icons \
    GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0" \
    PASSWORD_STORE_DIR="$XDG_DATA_HOME/password-store" \
    TEXMFCONFIG="$XDG_CONFIG_HOME/texlive/texmf-config" \
    TEXMFVAR="$XDG_CACHE_HOME/texlive/texmf-var" \
    TEXMFHOME="$XDG_DATA_HOME/texmf" \
    VIMINIT="source $XDG_CONFIG_HOME/vim/vimrc" \
    XSERVERRC="$XDG_CONFIG_HOME/X11/xserverrc" \
    MBSYNCRC="$XDG_CONFIG_HOME/isync/mbsyncrc" \
    XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" \
    HISTFILE="$XDG_STATE_HOME/bash/history" \
    XINITRC="$XDG_CONFIG_HOME/X11/xinitrc" \
    LESSHISTFILE="$XDG_DATA_HOME/lesshst" \
    ELECTRUMDIR="$XDG_DATA_HOME/electrum" \
    CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv" \
    WINEPREFIX="$HOME/.local/share/wine" \
    WGETRC="$HOME/.config/wget/wgetrc" \
    GNUPGHOME="$XDG_DATA_HOME/gnupg" \
    GOPATH="$XDG_DATA_HOME/go" \

alias wget="wget --hsts-file=$XDG_DATA_HOME/wget/wget-hsts"

export \
    HISTCONTROL=ignoreboth \
    HISTIZE= \
    HISTFILESIZE= \
    DATE=$(date -I) \
    WEEK=$(date '+%U') \

export \
    TERM="xterm-256color" \
    BROWSER="firefox-hardened" \
    EDITOR="nvim" \
    VISUAL="nvim" \

export \
    QT_QPA_PLATFORMTHEME="qt5ct" \
    CM_SELECTIONS="clipboard" \
    GTK_THEME="Materia:dark" \
    XCURSOR_THEME="Adwaita" \
    CM_MAX_CLIPS=10 \

export \
    GITLAB="ssh://git@gitlab.com/tavo-wasd" \
    HOMELAB="ssh://drive:/home/drive/drive" \
    BOOKMARKS="$HOME/Documents/bookmarks" \
    BIB="$HOME/Documents/bibliography" \
