#+title: Tavo's rc file
#+property: header-args :tangle bashrc

* Non-interactive

** PATH

#+begin_src shell
PATH="$HOME/.config/scripts/status${PATH:+:${PATH}}"
PATH="$HOME/.config/scripts/menu${PATH:+:${PATH}}"
PATH="$HOME/.config/scripts/menu/xclipmenu${PATH:+:${PATH}}"
PATH="$HOME/.config/scripts/sway${PATH:+:${PATH}}"
PATH="$HOME/.config/scripts/dwm${PATH:+:${PATH}}"
PATH="$HOME/.config/wrappers${PATH:+:${PATH}}"
PATH="$HOME/.config/scripts${PATH:+:${PATH}}"
PATH="$HOME/.local/bin${PATH:+:${PATH}}"
#PATH="/usr/local/plan9/bin${PATH:+:${PATH}}"
#+end_src

** Environment

#+begin_src shell
export \
    GITLAB="ssh://git@gitlab.com/tavo-wasd" \
    BOOKMARKS="$HOME/Documents/bookmarks" \
    BIB="$HOME/Documents/bibliography" \

[ -f ~/.config/shell/env ] && . ~/.config/shell/env
#+end_src

* Interactive

#+begin_src shell
case $- in
    *i*) ;;
      *) return;;
esac
#+end_src

** Bash-only config

#+begin_src bash
if ! shopt -oq posix; then
    for comp in /usr/share/bash-completion/bash_completion /etc/bash_completion; do
	[ -f $comp ] && . $comp && break
    done
fi

bind "set completion-ignore-case on"
shopt -s checkwinsize
shopt -s histappend
complete -cf doas
shopt -s cdspell
shopt -s autocd
set -o vi
#+end_src

** Prompt

#+begin_src shell
git_branch() {
    GIT_BRANCH="$(git branch 2>/dev/null | sed '/\*/!d;s/^\*\s*//g;s/\s*$//g')"
    [ -n "$GIT_BRANCH" ] && printf "%s " "$GIT_BRANCH"
}

[ -e "/data/data/com.termux" ]                && HOSTNAME=""
[ "${HOSTNAME:=$(hostname -s)}" = "laptop"  ] && HOSTNAME="󰌢"
[ "${HOSTNAME:=$(hostname -s)}" = "desktop" ] && HOSTNAME="󰇅"
#+end_src

*** Bash-only PS1

#+begin_src bash
PS1='
\[\e[48;5;7m\] \[\e[38;5;240m\]\[\e[48;5;7m\]$?\[\e[38;5;7m\]\[\e[48;5;108m\]\
\[\e[38;5;235m\]\[\e[48;5;108m\] ${HOSTNAME} \[\e[38;5;108m\]\[\e[48;5;66m\]\
\[\e[48;5;66m\]\[\e[38;5;235m\] \W \[\e[45;0m\]\[\e[38;5;66m\]\
\[\e[38;5;240m\] \@ \[\e[1;38;5;5m\]$(git_branch)\[\e[48;0m\]
\[\e[38;5;240m\] ╰─\$\[\e[0m\] '
#+end_src

** Aliases

#+begin_src shell
alias \
    src="cd $HOME/.local/src/ && ls" \
    cfg="cd $HOME/.config/ && ls" \
    tmp="cd $HOME/Desktop/temp/ && ls" \
    dsk="cd $HOME/Desktop/ && ls" \
    prj="cd $HOME/Desktop/projects/ && ls" \
    doc="cd $HOME/Documents/ && ls" \
    dow="cd $HOME/Downloads/ && ls" \
    mus="cd $HOME/Music/ && ls" \
    prt="cd $HOME/Pictures/Screenshots/ && ls" \
    bkg="cd $HOME/Pictures/Backgrounds/ && ls" \
    img="cd $HOME/Pictures/ && ls" \
    vid="cd $HOME/Videos/ && ls" \
    fzf="fzf --cycle --reverse" \
    diff="diff --color=auto" \
    grep="grep --color=auto" \
    calc="bc -l" \
    ls="ls -Alogh --color=auto --time-style=iso" \
    cp="cp -iv" \
    mv="mv -iv" \
    vim="nvim" \
    df-short="df -h | grep -v '\s/dev.*$\|\s/run.*$\|\s/boot.*$'" \
    qr-png="qrencode -s 16 -o qr.png" \
    qr="qrencode -t ansiutf8" \

if [ "${XDG_SESSION_TYPE}" = "wayland" ] ; then
    alias clip="wl-copy"
fi

if [ "${XDG_SESSION_TYPE}" = "x11" ] ; then
    alias clip="xsel -ib"
fi

! command -v sudo >/dev/null 2>&1 && alias sudo="doas"
command -v exa >/dev/null 2>&1 && alias ls="exa -al --icons --group-directories-first --no-permissions --no-user --time-style=iso --git"
command -v eza >/dev/null 2>&1 && alias ls="eza -al --icons --group-directories-first --no-permissions --no-user --time-style=iso --git"
command -v trash >/dev/null 2>&1 && alias rm="trash"
#+end_src
** Utils

#+begin_src shell

util_get_ssid() {
    [ -e "/sbin/iw" ] && iw="/sbin/iw"
    iw="${iw:=$(command -v iw)}"
    [ "${iw}" ] || return 1
    int="$(sed '/:/!d;s/^ *//;s/:.*$//' /proc/net/wireless)"
    if [ "$int" ] && grep 'up' /sys/class/net/w*/operstate >/dev/null 2>&1 ; then
        "${iw}" "${int}" link | sed '/SSID/!d;s/^.*: //'
    fi
}
#+end_src

** Startup

#+begin_src shell
if [ "$(tty)" = "/dev/tty1" ] ; then
    sleep 0.5
    amixer &
    #exec startx
    exec sway
fi

command -v fetch >/dev/null 2>&1 && fetch min
#+end_src
